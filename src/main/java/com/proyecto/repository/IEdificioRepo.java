package com.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.models.edificio;

@Repository
public interface IEdificioRepo extends JpaRepository<edificio, Integer> {
	// Busca por nombre
    @Query(value = "select * from edificio where edif_nombre=?",nativeQuery = true)
    edificio findbynombre(String edif_nopmbre);
    
    // Busca por id para editar
    @Query(value = "select * from edificio where edif_id=?",nativeQuery = true)
    edificio findbyId(int edif_id);

    // Eliminar por id
    @Transactional
    @Modifying
    @Query(value="delete from edificio where edif_id=?", nativeQuery =true)
    int deleteById(int edif_id);
}
