package com.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.proyecto.models.inventario;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

@Repository

public interface IInventarioRepo extends JpaRepository<inventario, Integer>{
    
        // Buscar por codigo de articulo
        @Query(value = "select a from inventario a where a.inv_art_codigo like %:text% or a.inv_doc_cedula like %:text% or a.inv_doc_nombre like %:text% or a.inv_doc_apellido like %:text% or a.inv_art_ubicacion_destino like %:text%")
        List<inventario> findbybuscar(@Param("text") String text);
        
        // Buscar por cedula
        @Query(value = "select * from inventario where inv_doc_cedula=?",nativeQuery = true)
        List<inventario> findbyCedula(String buscar);
        
        // Buscar para editar por id, retorna un solo valor
        @Query(value = "select * from inventario where inv_id=?",nativeQuery = true)
        inventario findbyId(int inv_id);
        
     // Buscar para editar por codigo, retorna un solo valor
        @Query(value = "select * from inventario where inv_art_codigo=?",nativeQuery = true)
        inventario findbyCodigo(String inv_art_codigo);
        
        // Eliminar por id
        @Transactional
        @Modifying
        @Query(value="delete from inventario where inv_id=?", nativeQuery =true)
        int deleteById(int inv_id);
        
}
