package com.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.proyecto.models.aula;

@Repository
public interface IAulaRepo extends JpaRepository<aula, Integer>{

	// Busca por nombre
    @Query(value = "select * from aula where aula_nombre=?",nativeQuery = true)
    aula findbynombre(String aula_nopmbre);
    
    // Busca por id para editar
    @Query(value = "select * from aula where aula_id=?",nativeQuery = true)
    aula findbyId(int edif_id);

    // Eliminar por id
    @Transactional
    @Modifying
    @Query(value="delete from aula where aula_id=?", nativeQuery =true)
    int deleteById(int cat_id);
}
