package com.proyecto.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.models.empresa;

@Repository
public interface IEmpresaRepo extends JpaRepository<empresa, Integer>{
	// Busca por ruc
    @Query(value = "select * from empresa where emp_ruc=?",nativeQuery = true)
    empresa findbyruc(String emp_ruc);
    
    // Busca por codigo nombre
    @Query(value = "select * from empresa where emp_nombre=?",nativeQuery = true)
    empresa findbynombre(String emp_nombre);

    // Busca por id para editar
    @Query(value = "select * from empresa where emp_id=?",nativeQuery = true)
    empresa findbyId(int emp_id);

    // Eliminar por id
    @Transactional
    @Modifying
    @Query(value="delete from empresa where emp_id=?", nativeQuery =true)
    int deleteById(int emp_id);
}
