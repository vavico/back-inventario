package com.proyecto.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Aula")
public class aula {
	@Id
    @Column(name="aula_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "AulaTable")
    private int aula_id;
	@Column(name = "aula_nombre", unique = true)
    private String aula_nombre;
	@Column(name = "aula_edif_id")
	private String aula_edif_id;
	
	public int getAula_id() {
		return aula_id;
	}
	public void setAula_id(int aula_id) {
		this.aula_id = aula_id;
	}
	public String getAula_nombre() {
		return aula_nombre;
	}
	public void setAula_nombre(String aula_nombre) {
		this.aula_nombre = aula_nombre;
	}
	public String getAula_edif_id() {
		return aula_edif_id;
	}
	public void setAula_edif_id(String aula_edif_id) {
		this.aula_edif_id = aula_edif_id;
	}
}
