package com.proyecto.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Edificio")
public class edificio {
	@Id
    @Column(name="edif_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "EdificioTable")
    private int edif_id;
	@Column(name = "edif_nombre", unique = true)
    private String edif_nombre;
	
	public int getEdif_id() {
		return edif_id;
	}
	public void setEdif_id(int edif_id) {
		this.edif_id = edif_id;
	}
	public String getEdif_nombre() {
		return edif_nombre;
	}
	public void setEdif_nombre(String edif_nombre) {
		this.edif_nombre = edif_nombre;
	}
}
