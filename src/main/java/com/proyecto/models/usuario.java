package com.proyecto.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class usuario {
	@Id
    @Column(name="usu_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "UsuarioTable")
    private int usu_id;
    @Column(name = "usu_usuario", unique=true)
    private String usu_usuario;
    @Column(name = "usu_password")
    private String usu_password;
    @Column(name = "usu_rol")
    private String usu_rol;
    
	public int getUsu_id() {
		return usu_id;
	}
    public void setUsu_id(int usu_id) {
		this.usu_id = usu_id;
	}
	public String getUsu_usuario() {
		return usu_usuario;
	}
	public void setUsu_usuario(String usu_usuario) {
		this.usu_usuario = usu_usuario;
	}
	public String getUsu_password() {
		return usu_password;
	}
	public void setUsu_password(String usu_password) {
		this.usu_password = usu_password;
	}
	public String getUsu_rol() {
		return usu_rol;
	}
	public void setUsu_rol(String usu_rol) {
		this.usu_rol = usu_rol;
	}
    
    
}
