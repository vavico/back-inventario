package com.proyecto.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "empresa")
public class empresa {
	@Id
    @Column(name="emp_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "EmpresaTable")
    private int emp_id;
    @Column(name = "emp_nombre", unique = true)
    private String emp_nombre;
	
    public int getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}
	public String getEmp_nombre() {
		return emp_nombre;
	}
	public void setEmp_nombre(String emp_nombre) {
		this.emp_nombre = emp_nombre;
	}
    
    
}
