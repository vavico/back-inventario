package com.proyecto.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "categorias")
public class categorias {
	@Id
    @Column(name="cat_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "CategoriaTable")
    private int cat_id;
	@Column(name = "cat_nombre", unique = true)
    private String cat_nombre;
    @Column(name = "cat_val_depre")
    private String cat_val_depre;
    @Column(name = "cat_tiempo_depre")
    private String cat_tiempo_depre;
	
    public int getCat_id() {
		return cat_id;
	}
	public void setCat_id(int cat_id) {
		this.cat_id = cat_id;
	}
	public String getCat_nombre() {
		return cat_nombre;
	}
	public void setCat_nombre(String cat_nombre) {
		this.cat_nombre = cat_nombre;
	}
	public String getCat_val_depre() {
		return cat_val_depre;
	}
	public void setCat_val_depre(String cat_val_depre) {
		this.cat_val_depre = cat_val_depre;
	}
	public String getCat_tiempo_depre() {
		return cat_tiempo_depre;
	}
	public void setCat_tiempo_depre(String cat_tiempo_depre) {
		this.cat_tiempo_depre = cat_tiempo_depre;
	}
    
    
}
