package com.proyecto.models;

// Clase para crear la tabla docente

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "docente")

public class docente {

    @Id
    @Column(name="doc_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "DocenteTable")
    private int doc_id;
    @Column(name = "doc_cedula", unique=true)
    private String doc_cedula;
    @Column(name = "doc_nombre")
    private String doc_nombre;
    @Column(name = "doc_apellido")
    private String doc_apellido;
    @Column(name = "doc_departamento")
    private String doc_departamento;
    @Column(name = "doc_telefono")
    private String doc_telefono;
    @Column(name = "doc_correo")
    private String doc_correo;

    public int getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(int doc_id) {
        this.doc_id = doc_id;
    }

    public String getDoc_cedula() {
        return doc_cedula;
    }

    public void setDoc_cedula(String doc_cedula) {
        this.doc_cedula = doc_cedula;
    }

    public String getDoc_nombre() {
        return doc_nombre;
    }

    public void setDoc_nombre(String doc_nombre) {
        this.doc_nombre = doc_nombre;
    }

    public String getDoc_apellido() {
        return doc_apellido;
    }

    public void setDoc_apellido(String doc_apellido) {
        this.doc_apellido = doc_apellido;
    }

    public String getDoc_departamento() {
        return doc_departamento;
    }

    public void setDoc_departamento(String doc_departamento) {
        this.doc_departamento = doc_departamento;
    }

    public String getDoc_telefono() {
        return doc_telefono;
    }

    public void setDoc_telefono(String doc_telefono) {
        this.doc_telefono = doc_telefono;
    }

    public String getDoc_correo() {
        return doc_correo;
    }

    public void setDoc_correo(String doc_correo) {
        this.doc_correo = doc_correo;
    }

}