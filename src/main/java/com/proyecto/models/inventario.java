package com.proyecto.models;
// Clase para crear la tabla inventario
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "inventario")
public class inventario {
    @Id
    @Column(name="inv_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "InventarioTable")
    private int inv_id;
    
    @Column(name = "inv_doc_cedula")
    private String inv_doc_cedula;
    
    @Column(name = "inv_art_codigo")
    private String inv_art_codigo;
    
    @Column(name = "inv_art_bien")
    private String inv_art_bien;
    
    @Column(name = "inv_art_color")
    private String inv_art_color;
    
    @Column(name = "inv_art_marca")
    private String inv_art_marca;
    
    @Column(name = "inv_art_modelo")
    private String inv_art_modelo;
    
    @Column(name = "inv_art_serie")
    private String inv_art_serie;
    
    @Column(name = "inv_art_estado")
    private String inv_estado;
    
    @Column(name = "inv_art_ubicacion_destino")
    private String inv_art_ubicacion_destino;
    
    @Column(name = "inv_fecha")
    private String inv_fecha;

    @Column(name = "inv_doc_nombre")
    private String inv_doc_nombre;
    
    @Column(name = "inv_doc_apellido")
    private String inv_doc_apellido;
        
	public int getInv_id() {
		return inv_id;
	}

	public void setInv_id(int inv_id) {
		this.inv_id = inv_id;
	}

	public String getInv_doc_cedula() {
		return inv_doc_cedula;
	}

	public void setInv_doc_cedula(String inv_doc_cedula) {
		this.inv_doc_cedula = inv_doc_cedula;
	}

	public String getInv_art_codigo() {
		return inv_art_codigo;
	}

	public void setInv_art_codigo(String inv_art_codigo) {
		this.inv_art_codigo = inv_art_codigo;
	}

	public String getInv_art_bien() {
		return inv_art_bien;
	}

	public void setInv_art_bien(String inv_art_bien) {
		this.inv_art_bien = inv_art_bien;
	}

	public String getInv_art_color() {
		return inv_art_color;
	}

	public void setInv_art_color(String inv_art_color) {
		this.inv_art_color = inv_art_color;
	}

	public String getInv_art_marca() {
		return inv_art_marca;
	}

	public void setInv_art_marca(String inv_art_marca) {
		this.inv_art_marca = inv_art_marca;
	}

	public String getInv_art_modelo() {
		return inv_art_modelo;
	}

	public void setInv_art_modelo(String inv_art_modelo) {
		this.inv_art_modelo = inv_art_modelo;
	}

	public String getInv_art_serie() {
		return inv_art_serie;
	}

	public void setInv_art_serie(String inv_art_serie) {
		this.inv_art_serie = inv_art_serie;
	}

	public String getInv_estado() {
		return inv_estado;
	}

	public void setInv_estado(String inv_estado) {
		this.inv_estado = inv_estado;
	}

	public String getInv_art_ubicacion_destino() {
		return inv_art_ubicacion_destino;
	}

	public void setInv_art_ubicacion_destino(String inv_art_ubicacion_destino) {
		this.inv_art_ubicacion_destino = inv_art_ubicacion_destino;
	}

	public String getInv_fecha() {
		return inv_fecha;
	}

	public void setInv_fecha(String inv_fecha) {
		this.inv_fecha = inv_fecha;
	}

	public String getInv_doc_nombre() {
		return inv_doc_nombre;
	}

	public void setInv_doc_nombre(String inv_doc_nombre) {
		this.inv_doc_nombre = inv_doc_nombre;
	}

	public String getInv_doc_apellido() {
		return inv_doc_apellido;
	}

	public void setInv_doc_apellido(String inv_doc_apellido) {
		this.inv_doc_apellido = inv_doc_apellido;
	}
}