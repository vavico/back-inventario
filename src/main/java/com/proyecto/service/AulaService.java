package com.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.models.aula;
import com.proyecto.repository.IAulaRepo;

@Service
public class AulaService {
	@Autowired
    private IAulaRepo aulaRepo;
    //guardar
    public aula crearAula(aula aula) {
        return aulaRepo.save(aula);
    }

    //listar
    public List<aula> listar(){
        return aulaRepo.findAll();
    }

    //buscar por nombre
    public aula buscarpornombre(String nombre) {
        return aulaRepo.findbynombre(nombre);
    }
    
    //buscar por id
    public aula buscarporid(int id) {
        return aulaRepo.findbyId(id);
    }
    
    //eliminar
    public void eliminarporId (int id){
    	aulaRepo.deleteById(id);
    }
}
