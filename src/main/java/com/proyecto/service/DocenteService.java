package com.proyecto.service;

//Clase de microservicio

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.proyecto.models.docente;
import com.proyecto.repository.IDocenteRepo;

@Service

public class DocenteService {
	
    @Autowired
        private IDocenteRepo docRepo;
        //guardar
        public docente crearDocente(docente doc) {
                return docRepo.save(doc);
        }
                
        //listar
        public List<docente> listar(){
                return docRepo.findAll();
        }
        
        //buscar por parametro, retorna una lista
        public List<docente> buscarporparametro(String parametro, String parametro2, String parametro3) {
            return docRepo.findbyparametro(parametro, parametro2, parametro3);
        }
        
        //buscar por id para editar, retorna un valor
        public docente buscarporid(int id) {
            return docRepo.findbyId(id);
        }
        	
        //eliminar por id
        public void eliminarporId (int id){
            docRepo.deleteById(id);
        }
}
