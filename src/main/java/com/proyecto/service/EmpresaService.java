package com.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.models.empresa;
import com.proyecto.repository.IEmpresaRepo;

@Service
public class EmpresaService {
	@Autowired
    private IEmpresaRepo empRepo;
    //guardar
    public empresa crearEmpresa(empresa emp) {
        return empRepo.save(emp);
    }

    //listar
    public List<empresa> listar(){
        return empRepo.findAll();
    }

    //buscar por usuario
    public empresa buscarpornombre(String nombre) {
        return empRepo.findbynombre(nombre);
    }

    //buscar por id
    public empresa buscarporid(int id) {
        return empRepo.findbyId(id);
    }
    
    //eliminar
    public void eliminarporId (int id){
        empRepo.deleteById(id);
    }
}
