package com.proyecto.service;

//Clase de microservicio

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.proyecto.models.inventario;
import com.proyecto.repository.IInventarioRepo;

@Service

public class InventarioService {
	
    @Autowired
        private IInventarioRepo invRepo;
    
        //guardar
        public inventario crearInventario(inventario inv) {
            return invRepo.save(inv);
        }
        
        public List<inventario> crearInv(List<inventario> inv) {
            return invRepo.saveAll(inv);
        }
        
        //listar
        public List<inventario> listar(){
            return invRepo.findAll();
        }
        
        //buscar
        public List<inventario> buscar(String buscar) {
            return invRepo.findbybuscar(buscar);
        }
        
        //buscar
        public List<inventario> buscarCedula(String buscar) {
            return invRepo.findbyCedula(buscar);
        }
        
        //buscar por id para eliminar, retorna un valor
        public inventario buscarporid(int id) {
            return invRepo.findbyId(id);
        }
        
      //buscar por id para modificar, retorna un valor
        public inventario buscarporCodigo(String codigo) {
            return invRepo.findbyCodigo(codigo);
        }
        
        //eliminar
        public void eliminarporId (int id){
            invRepo.deleteById(id);
        }
        
}