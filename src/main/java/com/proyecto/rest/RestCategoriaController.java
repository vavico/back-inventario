package com.proyecto.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto.models.categorias;
import com.proyecto.service.CategoriaService;

@RestController
@RequestMapping(path  = "/categoria")
public class RestCategoriaController {
	
	@Autowired
	private CategoriaService catSer;
	@PostMapping(path="/guardar")
	public categorias registrarCategoria(@Valid @RequestBody categorias cat){
		return catSer.crearCategoria(cat);
	}
	@GetMapping(path = "/listar")
	public List<categorias> listar(){
		return catSer.listar();
	}
	
	@GetMapping(path = "/listar/{nombre}")
	public categorias buscarpornombre(@PathVariable String nombre) {
		return catSer.buscarpornombre(nombre);
	}
	
	@DeleteMapping("eliminar/{id}")
	public ResponseEntity<?>delete(@PathVariable int id){
		categorias cat = catSer.buscarporid(id);
		if(cat==null){
			ResponseEntity.badRequest().build();
		}
		catSer.eliminarporId(id);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping(value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<categorias> updateCategoria(@PathVariable(value = "id") int id, @RequestBody categorias cat){
		categorias actualizar = catSer.buscarporid(id);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setCat_nombre(cat.getCat_nombre());
		actualizar.setCat_tiempo_depre(cat.getCat_tiempo_depre());
		actualizar.setCat_val_depre(cat.getCat_val_depre());
		return ResponseEntity.ok(catSer.crearCategoria(actualizar));
	}
}
