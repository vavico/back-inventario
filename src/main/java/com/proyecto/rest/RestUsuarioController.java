package com.proyecto.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto.models.usuario;
import com.proyecto.service.UsuarioService;

@RestController
@RequestMapping(path  = "/usuario")
public class RestUsuarioController {
	@Autowired
	private UsuarioService usuSer;
	@PostMapping(path="/guardar")
	public usuario registrarUsuario(@Valid @RequestBody usuario usu){
		return usuSer.crearUsuario(usu);
	}
	@GetMapping(path = "/listar")
	public List<usuario> listar(){
		return usuSer.listar();
	}
	
	@GetMapping(path = "/listar/{cedula}")
	public usuario buscarporcedula(@PathVariable String cedula) {
		return usuSer.buscarporcedula(cedula);
	}
        
    @GetMapping(path = "/listar/usuario/{usuario}")
	public usuario buscarporUsuario(@PathVariable String usuario) {
		return usuSer.buscarporusuario(usuario);
	}
	@DeleteMapping("eliminar/{id}")
	public ResponseEntity<?>delete(@PathVariable int id){
		usuario usu = usuSer.buscarporid(id);
		if(usu==null){
			ResponseEntity.badRequest().build();
		}
		usuSer.eliminarporId(id);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping(value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<usuario> updateUsuario(@PathVariable(value = "id") int id, @RequestBody usuario usu){
		usuario actualizar = usuSer.buscarporid(id);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setUsu_usuario(usu.getUsu_usuario());
		actualizar.setUsu_password(usu.getUsu_password());
		actualizar.setUsu_rol(usu.getUsu_rol());
		return ResponseEntity.ok(usuSer.crearUsuario(actualizar));
	}        
}
