package com.proyecto.rest;

//Clase de los metodos post

import java.util.List;

import com.proyecto.models.articulo;
import com.proyecto.models.inventario;
import com.proyecto.service.InventarioService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path  = "/inventario")
public class RestInventarioController {
	
	@Autowired
	private InventarioService invSer;
	@PostMapping(path="/guardar")
	public inventario registrarInventario(@Valid @RequestBody inventario inv){
		return invSer.crearInventario(inv);
	}
	@PostMapping(path="/guarda")
	public List<inventario> registrarInventario(@Valid @RequestBody List<inventario> inv){
		return invSer.crearInv(inv);
	}
	@GetMapping(path = "/listar")
	public List<inventario> listar(){
		return invSer.listar();
	}
	
	@GetMapping(path = "/listar/{buscar}")
	public List<inventario> buscar(@PathVariable String buscar) {
		return invSer.buscar(buscar);
	}
        
    @GetMapping(path = "/listar/cedula/{buscar}")
	public List<inventario> buscarCedula(@PathVariable String buscar) {
		return invSer.buscarCedula(buscar);
	}

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<?>delete(@PathVariable int id){
        inventario inv = invSer.buscarporid(id);
        if(inv==null){
            ResponseEntity.badRequest().build();
        }
        invSer.eliminarporId(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/editar/{codigo}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<inventario> updateInventario(@PathVariable(value = "codigo") String codigo, @RequestBody inventario inv){
		inventario actualizar = invSer.buscarporCodigo(codigo);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setInv_art_bien(inv.getInv_art_bien());
		actualizar.setInv_art_codigo(inv.getInv_art_codigo());
		actualizar.setInv_art_color(inv.getInv_art_color());
		actualizar.setInv_art_marca(inv.getInv_art_marca());
		actualizar.setInv_art_modelo(inv.getInv_art_modelo());
		actualizar.setInv_art_serie(inv.getInv_art_serie());
		actualizar.setInv_art_ubicacion_destino(inv.getInv_art_ubicacion_destino());
		actualizar.setInv_doc_apellido(inv.getInv_doc_apellido());
		actualizar.setInv_doc_cedula(inv.getInv_doc_cedula());
		actualizar.setInv_doc_nombre(inv.getInv_doc_nombre());
		actualizar.setInv_estado(inv.getInv_estado());
		actualizar.setInv_fecha(inv.getInv_fecha());
		return ResponseEntity.ok(invSer.crearInventario(actualizar));
	}
}