package com.proyecto.rest;

import java.util.List;

//Clase de los metodos post


import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.proyecto.models.articulo;
import com.proyecto.models.categorias;
import com.proyecto.service.ArticuloService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.springframework.data.domain.Pageable;


@RestController
@RequestMapping(path  = "/articulo")
public class RestArticuloController {
	
	@Autowired
	private ArticuloService artSer;
	
	@CrossOrigin
	@ResponseBody
	@PostMapping(path="/guardar")
	public articulo registrarArticulo(@Valid @RequestBody articulo art){
		return artSer.crearArticulo(art);
	}
	
	@GetMapping(path = "/listar")
	public Page<articulo> listar(Pageable page) {
        return this.artSer.listar(page);
    }
	
	@GetMapping(path = "/listarart")//lista los Artículos
    public List<articulo> ListarArt() {
    	return artSer.listarart();
    }
	
    @GetMapping(path = "/listart/{nombre}")
	public List<articulo> buscarporparam(@PathVariable String nombre) {
		return artSer.buscarpornombre(nombre);
	}
    
	@GetMapping(path = "/listar/{texto}")
	public Page<articulo> buscar(@PathVariable String texto, Pageable page) {
        return this.artSer.filtrar(texto, page);
    }
	
	@GetMapping(path = "/listar/asignado/{asignado}")
	public Page<articulo> buscarporAsignado(@PathVariable String asignado,Pageable page) {
		return artSer.verAsignado(asignado, page);
	}
    
	@GetMapping(path = "/listar/codigo/{codigo}")
	public articulo buscarporCodigo(@PathVariable String codigo) {
		return artSer.buscarporcodigo(codigo);
	}
	
	@DeleteMapping("eliminar/{id}")
	public ResponseEntity<?>delete(@PathVariable int id){
		articulo art = artSer.buscarporid(id);
		if(art==null){
			ResponseEntity.badRequest().build();
		}
		artSer.eliminarporId(id);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping(value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<articulo> updateArticulo(@PathVariable(value = "id") int id, @RequestBody articulo art){
		articulo actualizar = artSer.buscarporid(id);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setArt_codigo(art.getArt_codigo());
		actualizar.setArt_bien(art.getArt_bien());
		actualizar.setArt_bld(art.getArt_bld());
		actualizar.setArt_numero_acta(art.getArt_numero_acta());
		actualizar.setArt_asignado(art.getArt_asignado());
		actualizar.setArt_aula(art.getArt_aula());
		actualizar.setArt_categoria(art.getArt_categoria());
		actualizar.setArt_cod_anterior(art.getArt_cod_anterior());
		actualizar.setArt_color(art.getArt_color());
		actualizar.setArt_conciliacion(art.getArt_conciliacion());
		actualizar.setArt_cuenta_contable(art.getArt_cuenta_contable());
		actualizar.setArt_dimensiones(art.getArt_dimensiones());
		actualizar.setArt_empresa(art.getArt_empresa());
		actualizar.setArt_estado(art.getArt_estado());
		actualizar.setArt_fecha_depre(art.getArt_fecha_depre());
		actualizar.setArt_fecha_ing(art.getArt_fecha_ing());
		actualizar.setArt_marca(art.getArt_marca());
		actualizar.setArt_material(art.getArt_material());
		actualizar.setArt_modelo(art.getArt_modelo());
		actualizar.setArt_observacion(art.getArt_observacion());
		actualizar.setArt_serie(art.getArt_serie());
		actualizar.setArt_valor_contable(art.getArt_valor_contable());
		actualizar.setArt_descripcion(art.getArt_descripcion());
		return ResponseEntity.ok(artSer.crearArticulo(actualizar));
	}
        
        @PutMapping(value = "editar/codigo/{codigo}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<articulo> updateArticuloCodigo(@PathVariable(value = "codigo") String codigo, @RequestBody String art){
		articulo actualizar = artSer.buscarporcodigo(codigo);
		if(actualizar==null){
			ResponseEntity.badRequest().build();
		}
		actualizar.setArt_asignado(art);
		return ResponseEntity.ok(artSer.crearArticulo(actualizar));
	}
}