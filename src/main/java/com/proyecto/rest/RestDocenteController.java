package com.proyecto.rest;      

//Clase de los metodos post

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.proyecto.models.docente;
import com.proyecto.service.DocenteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
@RequestMapping(path  = "/docente")
public class RestDocenteController {
	
	@Autowired
	private DocenteService docSer;
	@PostMapping(path="/guardar")
	public docente registrarDocente(@Valid @RequestBody docente doc){
		return docSer.crearDocente(doc);
	}
	@GetMapping(path = "/listar")
	public List<docente> listar(){
		return docSer.listar();
	}
	
	@GetMapping(path = "/listar/{parametro}")
	public List<docente> buscarporparametro(@PathVariable String parametro) {
            String parametro2=parametro;
            String parametro3=parametro;
            return docSer.buscarporparametro(parametro,parametro2,parametro3);
	}
        
        @DeleteMapping("eliminar/{id}")
        public ResponseEntity<?>delete(@PathVariable int id){
            docente doc = docSer.buscarporid(id);
            if(doc==null){
                ResponseEntity.badRequest().build();
            }
            docSer.eliminarporId(id);
            return ResponseEntity.ok().build();
        }
                
        @PutMapping(value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseStatus(HttpStatus.OK)
        public ResponseEntity<docente> updateDocente(@PathVariable(value = "id") int id, @RequestBody docente doc){
            docente actualizar = docSer.buscarporid(id);
            if(actualizar==null){
                ResponseEntity.badRequest().build();
            }
            actualizar.setDoc_cedula(doc.getDoc_cedula());
            actualizar.setDoc_nombre(doc.getDoc_nombre());
            actualizar.setDoc_apellido(doc.getDoc_apellido());
            actualizar.setDoc_correo(doc.getDoc_correo());
            actualizar.setDoc_departamento(doc.getDoc_departamento());
            actualizar.setDoc_telefono(doc.getDoc_telefono());
            return ResponseEntity.ok(docSer.crearDocente(actualizar));
        }
        
}